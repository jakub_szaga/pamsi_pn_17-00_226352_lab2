#include "stdafx.h"
#include "iostream"
#include <string>
using namespace std;

class element {
public:
	char liczba;
	class element *nast;
	element();
};

element::element() {
	nast = NULL;
}

class deque {
private:
	element *begin;
public:
	void dodaj_tyl(char liczba);
	void dodaj_przod(char liczba);
	void usun_wszystko();
	void usun_przod();
	void usun_tyl();
	void wyswietl();
	float first();
	float last();
	int size();
	deque();
};

deque::deque() {
	begin = NULL;
}

//deque *list = new dque;

float deque::first()
{
	return begin->liczba;
}

float deque::last()
{
	element *temp = begin;
	
	while (temp->nast)
		temp = temp->nast;

	return temp->liczba;
}

int deque::size()
{
	element *temp = new element;
	int size=0;
	temp = begin;
	while (temp != NULL)
	{
		size++;
		temp = temp->nast;
	}
	return size;
}

void deque::usun_przod() {
	element *temp = new element;
	if (begin == NULL) {
		cout << "Kolejka jest pusta." << endl;;
	}
	else {
		temp = begin;
		//cout << "Usunieto: " << temp->liczba << endl;
		begin = begin->nast;
		delete temp;
	}
}

void deque::usun_tyl() {
	if (begin != NULL)
	{
		if (begin->nast == NULL)
			delete begin;
		else
		{
			element *temp = begin;
			element *usun;

			while (temp->nast->nast)
				temp = temp->nast;

			usun = temp->nast;
			temp->nast = NULL;
			delete usun;
		}
	}
}

void deque::usun_wszystko() {
	while (begin != 0)
	{
		//list->begin = 0;
		begin = begin->nast;
	}
}

void deque::dodaj_przod(char liczba)
{
	element *nowy = new element;
	nowy->liczba = liczba;
	if (begin == 0)
		begin = nowy;
	else
	{
		element *temp = begin;
		nowy->nast = temp;
		begin = nowy;
	}
}

void deque::dodaj_tyl(char liczba) {

	element *nowy = new element;
	nowy->liczba = liczba;
	if (begin == 0)
	{
		begin = nowy;
	}
	else
	{
		element *temp = begin;

		while (temp->nast)
		{
			temp = temp->nast;
		}
		temp->nast = nowy;
		nowy->nast = 0;
	}
}

void deque::wyswietl() {
	element *proba = new element;
	proba = begin;
	if (begin == NULL) {
		cout << "Kolejka jest pusta." << endl;;
	}
	else
	{
		//for (int i = 0; i < rozmiar; i++)
		while (proba != NULL)
		{
			cout << proba->liczba << endl;
			proba = proba->nast;
		}
	}
}

bool jestPal(deque *list)
{
	// Dla rozmiaru 0 lub 1 wyraz jest palindromem
	if ((list->size() == 0) || (list->size() == 1))
		return true;
	else
	{
		// Gdy ostatni i pierwszy znak są różne to wyraz nie jest palindromem
		if (list->first() != list->last())
			return false;
		else
		{
			list->usun_tyl();
			list->usun_przod();
			return jestPal(list);
		}
	}
}

int main()
{
	string wyraz;
	//kolejka *list = new kolejka;
	deque *list = new deque;
	element *temp = new element;

	cout << "Podaj swoj palindrom:" << endl;
	cin >> wyraz;
	cout << endl;
	for (int i = 0; i < wyraz.size(); i++)
	{
		list->dodaj_tyl(wyraz[i]);
	}
	cout << "Oto podane slowo:" << endl;
	list->wyswietl();
	//list->usun_tyl();
	
	if (jestPal(list) == true)
		cout << "To jest palindrom!" << endl;
	else
		cout << "To nie jest palindrom." << endl;

	//cout << list->begin->liczba << endl;
	//cout << list->begin->nast->liczba << endl;

	//list->wyswietl(proba);
	delete(list);



	return 0;
}